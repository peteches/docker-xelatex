FROM fedora:latest

LABEL description="Fedora image with xelatex dependencies preinstalled"

RUN dnf install -y \
	fontawesome-fonts \
	git \
	texlive-collection-fontsrecommended \
	texlive-euenc \
	texlive-fancyhdr \
	texlive-latex \
	texlive-metafont \
	texlive-parskip \
	texlive-ragged2e \
	texlive-sourcesanspro \
	texlive-tcolorbox \
	texlive-texconfig \
	texlive-xetex-bin \
	texlive-xetex-def \
	texlive-xifthen
